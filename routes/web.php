<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
    return view('homepage.homepage');
});
Route::get('/about', function () {
    return view('homepage.about');
});
Route::get('/test', function () {
    return view('homepage.test');
});
Route::get('/contact', function () {
    return view('homepage.contact');
});
Route::get('/catalogue', function () {
    return view('homepage.catalogue');
});
Route::get('/tin-tuc', function () {
    return view('homepage.tintuc');
});
Route::get('/tu-van', function () {
    return view('homepage.tuvan');
});
Route::get('/vong-bi-skf', function () {
    return view('products.vongbiskf');
});
Route::get('/mo-boi-tron', function () {
    return view('products.moboitron');
});
Route::get('/dung-cu', function () {
    return view('products.dungcu');
});
Route::get('/giao-hang', function () {
    return view('homepage.giaohang');
});
Route::get('/bao-mat', function () {
    return view('homepage.baomat');
});
Route::get('/thanh-toan', function () {
    return view('homepage.thanhtoan');
});
Route::get('/phan-biet-hang-gia', function () {
    return view('homepage.phanbietvongbi');
});
