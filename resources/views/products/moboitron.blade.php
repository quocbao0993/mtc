@extends('master')
@section('content')
<section class="sec-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 ">
                <h4 class="section-title-7"><span class="roboto-slab uppercase">MỠ BÔI TRƠN</span></h4>
            </div>
            <!--end title-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/5465/mo-lgfq2-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/vong-bi-skf">Mỡ SKF LGFQ 2</a>
                </h5>
                <p>Mỡ thực phẩm chịu tải nặng</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/5424/lagf18-50-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">Bơm nạp mỡ </a>
                </h5>
                <p>SKF LAGF 18, LAGF 50</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/5423/bom-mo-cam-tay-skf-lagh-400-300x224c.png"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">Súng bơm mỡ </a>
                </h5>
                <p> LAGH 400</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/5418/lagg18m-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">Thiết bị bơm mỡ </a>
                </h5>
                <p>LAGG Seri</p>
                <h5 class="text-red-4"></h5>
                <br>
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/4781/sung-bom-mo-skf-tlgh1-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">Súng bơm mỡ </a>
                </h5>
                <p>TLGH 1</p>
                <h5 class="text-red-4"></h5>
                <br>

            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/4011/mo-skf-lnmt3ajpg-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html"> Mỡ đa năng SKF</a>
                </h5>
                <p> Mỡ SKF LNMT 3A</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/3598/mo-lgem2-skf-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html"> Mỡ EP với phụ gia Disulphite</a>
                </h5>
                <p>Mỡ SKF LGEM 2</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/3191/mo-lgep2-skf-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">Mỡ SKF </a>
                </h5>
                <p>LGEP 2</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/3192/mo-lagd-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">Mỡ tự động SKF System 24 </a>
                </h5>
                <p>Mỡ SKF LAGD </p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/3194/mo-lghp2-skf-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">Mỡ chịu nhiệt độ cao</a>
                </h5>
                <p>Mỡ SKF LGHP 2</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/3194/mo-lghp2-skf-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">Mỡ chịu nhiệt độ cao</a>
                </h5>
                <p> CMỡ SKF LGHP 2 </p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/3195/mo-lghb2-skf-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">Mỡ độ nhớt cao, nhiệt độ cao</a>
                </h5>
                <p> Mỡ SKF LGHB 2</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/3196/mo-lgwa2-skf-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html"> Mỡ dãy nhiệt độ rộng

                </a>
                </h5>
                <p>Mỡ SKF LGWA 2</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/3197/mo-lgmt3-skf-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html"> Mỡ đa năng SKF
                </a>
                </h5>
                <p> SKF LGMT 3</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/3198/mo-lgmt2-skf-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">Mỡ đa năng SKF </a>
                </h5>
                <p>Mỡ SKF LGMT 2</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

        </div>
    </div>
</section>
@endsection
