@extends('master')
@section('content')
<section class="sec-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 ">
                <h4 class="section-title-7"><span class="roboto-slab uppercase">VÒNG BI SKF</span></h4>
            </div>
            <!--end title-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7050/vong-bi-cau-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/vong-bi-skf">VÒNG BI CẦU SKF</a>
                </h5>
                <p>Deep groove ball bearings</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7052/vong-bi-cau-tua-lua-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">VÒNG BI ĐỠ TỰ LỰA SKF</a>
                </h5>
                <p> Self-aligning ball bearings</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7051/vong-bi-do-chan-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">VÒNG BI ĐỠ CHẶN SKF</a>
                </h5>
                <p> Angular contact ball bearings</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7048/vong-bi-tiep-xuc-4-diem-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">VÒNG BI TIẾP XÚC BỐN ĐIỂM SKF</a>
                </h5>
                <p>Angular contact ball bearings, four-point contact ball bearings</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7064/vong-bi-tiep-xuc-2-day-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">VÒNG BI TIẾP XÚC GÓC 2 DÃY</a>
                </h5>
                <p> Angular contact ball bearings, double row</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7055/vong-bi-chan-truc-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">VÒNG BI CHẶN TRỤC SKF</a>
                </h5>
                <p> Thrust ball bearings</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7049/vong-bi-con-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">VÒNG BI CÔN SKF</a>
                </h5>
                <p>Tapered roller bearings</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7056/vong-bi-tang-trong-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">VÒNG BI TANG TRỐNG 2 DÃY TỰ LỰA </a>
                </h5>
                <p>Spherical roller bearings</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7117/vong-bi-tang-trong-chan-truc-skf-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">VÒNG BI TANG TRỐNG CHẶN TRỤC </a>
                </h5>
                <p>Spherical roller thrust bearings </p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7057/vong-bi-mat-cau-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">VÒNG BI MẶT CẦU SKF </a>
                </h5>
                <p>Radial spherical plain bearings</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7053/vong-bi-dua-do-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">VÒNG BI ĐŨA ĐỠ SKF</a>
                </h5>
                <p> Cylindrical roller bearings </p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7059/vong-bi-yar-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">VÒNG BI YAR</a>
                </h5>
                <p> Insert bearings</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7058/vong-bi-kim-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">VÒNG BI KIM </a>
                </h5>
                <p>Needle roller bearings</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7063/vong-bi-con-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">VÒNG BI XE TẢI </a>
                </h5>
                <p> Truck & Trailer Wheel</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7061/ong-lot-con-skf-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">ỐNG LÓT VÒNG BI </a>
                </h5>
                <p>Adapter sleeves</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7695/vong-bi-carb-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">VÒNG BI CARB SKF </a>
                </h5>
                <p>CARB toroidal roller bearings</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7062/vong-bi-xe-may-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">VÒNG BI XE MÁY  </a>
                </h5>
                <p>Genio - Enduro</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7056/vong-bi-tang-trong-150x150c.jpg?v=2.5.9"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">VÒNG BI TANG TRỐNG 2 DÃY TỰ LỰA </a>
                </h5>
                <p>Spherical roller bearings</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->
        </div>
    </div>
</section>
@endsection
