@extends('master')
@section('content')

<section class="sec-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 ">
                <h4 class="section-title-7"><span class="roboto-slab uppercase">Dụng cụ thiết bị bảo trì SKF chính hãng</span></h4>
            </div>
            <!--end title-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/8123/tktl31-3-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/vong-bi-skf">Súng đo nhiệt độ </a>
                </h5>
                <p>SKF TKTL 31</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/8014/tktl21-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">Súng đo nhiệt độ  </a>
                </h5>
                <p>SKF TKTL 21 </p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7976/tktl11-sung-do-nhiet-do-skf-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">Súng đo nhiệt độ</a>
                </h5>
                <p> KF TKTL 11</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/7549/0901d196801330ab-microlog-ax-cmxa80-1-tcm-12-30320-300x224c.png"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">Máy phân tích rung động  </a>
                </h5>
                <p>SKF CMXA</p>
                <h5 class="text-red-4"></h5>
                <br>
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/5466/20180313134404-premio-300x224c.png"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">Dụng cụ đo lực căng dây đai</a>
                </h5>
                <p>PHG PT/C1 007 </p>
                <h5 class="text-red-4"></h5>
                <br>

            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/3584/dung-cu-can-chinh-vong-bi-skf-tmas-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html"> Dụng cụ căn chỉnh máy  </a>
                </h5>
                <p> TMAS </p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/3585/bom-thuy-luc-skf-tmjl-100-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html"> Bơm thủy lực </a>
                </h5>
                <p>SKF TMJL 100-100 MPa</p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="women-product-preview.html">
                        <div class="image-holder">
                            <div class="hoverbox"><i class="fa fa-link"></i></div>
                            <img src="https://ngocanh.com/public/uploads/images/3591/dung-cu-do-do-on-tmst-3-skf-300x224c.jpg"
                                alt="" class="img-responsive">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="women-product-preview.html">Dụng cụ đo độ ồn  </a>
                </h5>
                <p>TMST 3 SKF </p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

        </div>
    </div>
</section>
@endsection
