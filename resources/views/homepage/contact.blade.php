<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<!--<![endif]-->
<!--<![endif]-->
<html lang="en">
<head>
<title>MTC - ĐẠI LÝ SKF CHÍNH HÃNG TẠI MIỀN TRUNG</title>
<meta charset="utf-8">
<!-- Meta -->
<meta name="keywords" content="" />
<meta name="author" content="">
<meta name="robots" content="" />
<meta name="description" content="" />

<!-- this styles only adds some repairs on idevices  -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon.ico">

<!-- Google fonts - witch you want to use - (rest you can just remove) -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Stylesheets -->
<link rel="stylesheet" media="screen" href="/assets/js/bootstrap/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="/assets/js/mainmenu/menu.css" type="text/css" />
<link rel="stylesheet" href="/assets/css/default.css" type="text/css" />
<link rel="stylesheet" href="/assets/css/layouts.css" type="text/css" />
<link rel="stylesheet" href="/assets/css/shortcodes.css" type="text/css" />
<link rel="stylesheet" href="/assets/css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" media="screen" href="/assets/css/responsive-leyouts.css" type="text/css" />
<link rel="stylesheet" href="/assets/js/masterslider/style/masterslider.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/Simple-Line-Icons-Webfont/simple-line-icons.css" media="screen" />
<link rel="stylesheet" href="/assets/css/et-line-font/et-line-font.css">
<link rel="stylesheet" href="css/colors/lightblue.css" />
<link rel="stylesheet" type="text/css" href="/assets/js/smart-forms/smart-forms.css">



<!-- Remove the below comments to use your color option -->
<!--<link rel="stylesheet" href="css/colors/lightblue.css" />-->
<!--<link rel="stylesheet" href="css/colors/orange.css" />-->
<!--<link rel="stylesheet" href="css/colors/green.css" />-->
<!--<link rel="stylesheet" href="css/colors/pink.css" />-->
<!--<link rel="stylesheet" href="css/colors/red.css" />-->
<!--<link rel="stylesheet" href="css/colors/purple.css" />-->
<!--<link rel="stylesheet" href="css/colors/bridge.css" />-->
<!--<link rel="stylesheet" href="css/colors/yellow.css" />-->
<!--<link rel="stylesheet" href="css/colors/violet.css" />-->
<!--<link rel="stylesheet" href="css/colors/cyan.css" />-->
<!--<link rel="stylesheet" href="css/colors/mossgreen.css" />-->

</head>

<body>
    <div class="site_wrapper">
        <div class="topbar dark topbar-padding">
          <div class="container">
            <div class="topbar-left-items">
              <ul class="toplist toppadding pull-left paddtop1">
                <li class="rightl">Chăm sóc khách hàng - HOTLINE:</li>
                <li>0982488345 - 0913665565 - 0397900540</li>

                {{-- <li>EMAIL: XXX</li> --}}

              </ul>
            </div>
            <!--end left-->

            <div class="topbar-right-items pull-right">
              <ul class="toplist toppadding">
                {{-- <li><a href="#"><i class="fa fa-search"></i> &nbsp;</a></li> --}}
                <li>ĐẠI LÝ ỦY QUYỀN SKF TẠI VIỆT NAM</li>
                <br>
                <li><i class="fa fa-envelope"> Email: email@vongbimientrung.com</i></li>
                                                                                                                                                                                                                   </ul>
            </div>
          </div>
        </div>
        <div class="topbar white">
          <div class="container">
            <div class="topbar-left-items">
              <div class="margin-top1"></div>
              <ul class="toplist toppadding pull-left paddtop1">
                <li class="lineright"><a href="/#">Đăng nhập</a></li>
                <li><a href="/#">Đăng ký</a></li>
              </ul>
             </div>
            <!--end left-->

            <div class="topbar-middle-logo no-bgcolor"><a href="/#"><img src="/assets/images/logo.png" alt=""/></a></div>

            <!--end middle-->

            <div class="topbar-right-items pull-right">
              <div class="margin-top1"></div>
              <ul class="toplist toppadding">
                <li class="lineright"><a href="/#">Tìm kiếm</a></li>
                <li class="last"><a href="/#">Nhận báo giá</a></li>
                {{-- <li class="last"><a href="#">Giỏ hàng</a></li> --}}
              </ul>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>

        <div id="header">
          <div class="container">
            <div class="navbar red-2 navbar-default yamm ">
              <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid" class="navbar-toggle two three"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
              </div>
              <div id="navbar-collapse-grid" class="navbar-collapse collapse">
                <ul class="nav red-2 navbar-nav">
                  <li class="dropdown"> <a href="/#" class="dropdown-toggle active">TRANG CHỦ</a>
                  </li>
                  <li><a href="/about" class="dropdown-toggle">GIỚI THIỆU</a></li>
                  <li class="dropdown yamm-fw"> <a href="/#" class="dropdown-toggle">SẢN PHẨM</a>
                    <ul class="dropdown-menu">
                      <li>
                        <!-- Content container to add padding -->
                        <div class="yamm-content">
                          <div class="row">
                            <ul class="col-sm-6 col-md-3 list-unstyled ">
                              <li>
                                <p> SẢN PHẨM SKF CHÍNH HÃNG </p>
                              </li>
                              <li><a href="/vong-bi-skf"><img width="58" height="58" alt="Vòng bi SKF chính hãng" src="https://ngocanh.com/public/uploads/images/5435/vongbi-skf-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; VÒNG BI SKF CHÍNH HÃNG</a></li>
                              <li><a href="/#"><img width="58" height="58" alt="Gối đỡ SKF chính hãng" src="https://ngocanh.com/public/uploads/images/100/goi-do-skf-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; GỐI ĐỠ SKF CHÍNH HÃNG</a></li>
                              <li><a href="/mo-boi-tron"><img width="58" height="58" alt="Mỡ bôi trơn SKF chính hãng" src="https://ngocanh.com/public/uploads/images/103/mo-skf-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; MỠ BÔI TRƠN SKF CHÍNH HÃNG</a></li>
                              <li><a href="/#"><img width="58" height="58" alt="Dây đai SKF chính hãng" src="https://ngocanh.com/public/uploads/images/1363/day-dai-skf-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; DÂY ĐAI SKF CHÍNH HÃNG</a></li>
                              <li><a href="/#"><img width="58" height="58" alt="Phớt chặn dầu SKF chính hãng" src="https://ngocanh.com/public/uploads/images/1364/phot-skf-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; PHỚT CHẶN DẦU SKF CHÍNH HÃNG</a></li>
                            </ul>
                            <ul class="col-sm-6 col-md-3 list-unstyled ">
                              <li>
                                <p> DỤNG CỤ BẢO TRÌ SKF CHÍNH HÃNG </p>
                              </li>
                              <li><a href="/dung-cu"><img width="58" height="58" alt="Dụng cụ bảo trì SKF chính hãng" src="https://ngocanh.com/public/uploads/images/1365/dung-cu-skf-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; DỤNG CỤ BẢO TRÌ SKF CHÍNH HÃNG</a></li>
                            </ul>
                            <ul class="col-sm-6 col-md-3 list-unstyled ">
                              <li>
                                <p> Xích tải SFK CHỈNH HÃNG </p>
                              </li>
                              <li><a href="/#"><img width="58" height="58" alt="Xích tải SKF chính hãng" src="https://ngocanh.com/public/uploads/images/5432/xich-tai-skf-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; XÍCH TẢI SKF CHÍNH HÃNG</a></li>
                            </ul>
                            <ul class="col-sm-6 col-md-3 list-unstyled ">
                              <li>
                                <p> VÒNG BI XE CHÍNH HÃNG </p>
                              </li>
                              <li><a href="/#"><img width="58" height="58" alt="Vòng bi xe máy chính hãng" src="https://ngocanh.com/public/uploads/images/5433/vong-bi-xe-may-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; VÒNG BI XE MÁY SKF CHÍNH HÃNG</a></li>
                              <li><a href="/#"><img width="58" height="58" alt="Vòng bi xe tải chính hãng" src="https://ngocanh.com/public/uploads/images/5434/vong-bi-xe-tai-skf-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; VÒNG BI XE TẢI SKF CHÍNH HÃNG</a></li>
                            </ul>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </li>
                  <li class="dropdown yamm-fw"> <a href="/#" class="dropdown-toggle">TÀI LIỆU</a>
                    <ul class="dropdown-menu">
                      <li>
                        <!-- Content container to add padding -->
                        <div class="yamm-content">
                          <div class="row">
                            <ul class="col-sm-6 col-md-3 list-unstyled ">
                              <li>
                                <a href="/tin-tuc"> Tin tức chung SKF </a>
                              </li>
                              {{-- <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Premium Wear</a></li> --}}
                            </ul>
                            <ul class="col-sm-6 col-md-3 list-unstyled ">
                              <li>
                                <a href="/tu-van"> Tư vấn - Review </a>
                              </li>
                              {{-- <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Formal Shoes</a></li>
                              <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Sandals & Floaters</a></li>
                              <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Boots</a></li>
                              <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Casual Shoes</a></li>
                              <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Sports Shoes</a></li> --}}
                            </ul>
                            {{-- <ul class="col-sm-6 col-md-3 list-unstyled ">
                              <li>
                                <p> Watches </p>
                              </li>
                              <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Fasttrack</a></li>
                              <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Casio</a></li>
                              <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Timex</a></li>
                              <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Nixon</a></li>
                              <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Nine West</a></li>
                            </ul>
                            <ul class="col-sm-6 col-md-3 list-unstyled ">
                              <li>
                                <p> Accessories </p>
                              </li>
                              <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Bags</a></li>
                              <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Belts</a></li>
                              <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Sunglasses</a></li>
                              <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Shaving Kits & Needs</a></li>
                              <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Bath Needs</a></li>
                            </ul> --}}
                          </div>
                        </div>
                      </li>
                    </ul>
                  </li>
                  <li><a href="/catalogue" class="dropdown-toggle">CATALOGUE</a></li>

                  <li><a href="/contact" class="dropdown-toggle">LIÊN HỆ</a></li>
                </ul>
                <br/>
                <a href="/contact" class="dropdown-toggle pull-right btn btn-red btn-xround">GỌI NGAY</a> </div>
            </div>
          </div>
        </div>



  <section class="sec-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-8">

        <div class="smart-forms bmargin">
            <h3 class=" roboto-slab">Liên hệ</h3>
            <p>Cảm ơn bạn đã quan tâm tới sản phẩm SKF chính hãng. Nếu bạn có bất kỳ thắc mắc nào về sản phẩm SKF chính hãng. Hãy liên hệ với chúng tôi. </p>
            <br>
            Đại lý SKF chính hãng tại Việt Nam
            Add: 04 Trần Kế Xế Xương, Tổ 10, Phường Hải Châu II, Quận Hải Châu, Thành phố Đà Nẵng, Việt Nam
            Phone: 0982 488 345 - 0913 66 55 65 - 0397900540
            <br>
            <br>
            Vui lòng để lại thông tin và chúng tôi sẽ liên hệ lại với bạn sớm nhất
            <form method="post" action="php/smartprocess.php" id="smart-form" novalidate="novalidate">
              <div>
                <div class="section">
                  <label class="field prepend-icon">
                    <input type="text" name="sendername" id="sendername" class="gui-input" placeholder="Nhập tên của bạn">
                    <span class="field-icon"><i class="fa fa-user"></i></span> </label>
                </div>
                <!-- end section -->

                <div class="section">
                  <label class="field prepend-icon">
                    <input type="email" name="emailaddress" id="emailaddress" class="gui-input" placeholder="Nhập địa chỉ email">
                    <span class="field-icon"><i class="fa fa-envelope"></i></span> </label>
                </div>
                <!-- end section -->

                <div class="section colm colm6">
                  <label class="field prepend-icon">
                    <input type="tel" name="telephone" id="telephone" class="gui-input" placeholder="Nhập số điện thoại">
                    <span class="field-icon"><i class="fa fa-phone-square"></i></span> </label>
                </div>
                <!-- end section -->

                <div class="section">
                  <label class="field prepend-icon">
                    <input type="text" name="sendersubject" id="sendersubject" class="gui-input" placeholder="Tiêu đề">
                    <span class="field-icon"><i class="fa fa-lightbulb-o"></i></span> </label>
                </div>
                <!-- end section -->

                <div class="section">
                  <label class="field prepend-icon">
                    <textarea class="gui-textarea" id="sendermessage" name="sendermessage" placeholder="Nhập lời nhắn"></textarea>
                    <span class="field-icon"><i class="fa fa-comments"></i></span> <span class="input-hint"> <strong>Hint:</strong> Vui lòng nhập dưới 300 ký tự.</span> </label>
                </div>
                <!-- end section -->

                <!--<div class="section">
                            <div class="smart-widget sm-left sml-120">
                                <label class="field">
                                    <input type="text" name="captcha" id="captcha" class="gui-input sfcode" maxlength="6" placeholder="Enter CAPTCHA">
                                </label>
                                <label class="button captcode">
                                    <img src="php/captcha/captcha.php?<?php echo time();?>" id="captchax" alt="captcha">
                                    <span class="refresh-captcha"><i class="fa fa-refresh"></i></span>
                                </label>
                            </div>
                        </div>-->

                <div class="result"></div>
                <!-- end .result  section -->

              </div>
              <!-- end .form-body section -->
              <div class="form-footer">
                <button type="submit" data-btntext-sending="Sending..." class="button btn-primary red-4">Gởi thông tin</button>
                <button type="reset" class="button"> Hủy bỏ </button>
              </div>
              <!-- end .form-footer section -->
            </form>
          </div>

        </div>
        <!--end left-->

        <div class="col-md-4 bmargin">
            <h3 class="">Thông tin liên hệ</h3>

            <h6><strong>MTC COMPANY</strong></h6>
            04 Trần Kế Xương, thành phố Đà Nẵng, Việt Nam <br>
            Phone: 0982488345 - 0913665565 - 0397900540<br>
            {{-- FAX: +1 0123-4567-8900<br> --}}
            <br>
            E-mail: <a href="mailto:email@example.com">support@vongbimientrung.com</a><br>
            Website: <a href="/#">www.vongbimientrung.com</a>
            <div class="clearfix"></div>
            <br>
            <h3 class="love-ya-like-a-sister">Tìm địa chỉ</h3>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3833.9640660777372!2d108.21529691518701!3d16.067354388882173!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314218345659e309%3A0x92695eb6e98f5b0e!2zMDQgVHLhuqduIEvhur8gWMawxqFuZywgSOG6o2kgQ2jDonUgMiwgSOG6o2kgQ2jDonUsIMSQw6AgTuG6tW5nIDU1MDAwMCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1661460472145!5m2!1svi!2s" width="450" height="320" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
          </div>
        <!--end right-->
      </div>
    </div>
  </section>
<!-- end site wraper -->

<!-- ============ JS FILES ============ -->

<script type="text/javascript" src="/assets/js/universal/jquery.js"></script>
<script src="/assets/js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/js/masterslider/jquery.easing.min.js"></script>
<script src="/assets/js/masterslider/masterslider.min.js"></script>
<script type="text/javascript">
(function($) {
 "use strict";
	var slider = new MasterSlider();
	// adds Arrows navigation control to the slider.
	slider.control('arrows');
	slider.control('bullets');

	slider.setup('masterslider' , {
		 width:1600,    // slider standard width
		 height:630,   // slider standard height
		 space:0,
		 speed:45,
		 layout:'fullwidth',
		 loop:true,
		 preload:0,
		 autoplay:true,
		 view:"parallaxMask"
	});

})(jQuery);
</script>
<script src="/assets/js/mainmenu/customeUI.js"></script>
<script src="/assets/js/scrolltotop/totop.js"></script>
<script src="/assets/js/mainmenu/jquery.sticky.js"></script>
<script src="/assets/js/pagescroll/animatescroll.js"></script>

<script src="/assets/js/scripts/functions.js" type="text/javascript"></script>
</body>
</html>
