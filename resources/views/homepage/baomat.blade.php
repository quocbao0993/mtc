@extends('master')
@section('content')
<div class="about_section">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12">
                <div class="about_content">
                    <p><strong><span style="font-size: medium;">Chào mừng bạn đến với website vongbimientrung.com<br></span></strong><br><span style="font-size: medium;"> Chính sách bảo mật này nhằm giúp Quý khách hiểu về cách website thu thập và sử dụng thông tin cá nhân của mình thông qua việc sử dụng trang web, bao gồm mọi thông tin có thể cung cấp thông qua trang web khi Quý khách đăng ký tài khoản, đăng ký nhận thông tin liên lạc từ MTCvietnam.vn, hoặc khi Quý khách mua sản phẩm, dịch vụ, yêu cầu thêm thông tin dịch vụ từ MTC Việt Nam.</span></p>
<p><span style="font-size: medium;">1 - Mục đích thu thập thông tin cá nhân của Khách hàng</span></p>
<p><span style="font-size: medium;">Cung cấp dịch vụ cho Khách hàng và quản lý, sử dụng thông tin cá nhân của Khách hàng nhằm mục đích quản lý cơ sở dữ liệu về Khách hàng và kịp thời xử lý các tình huống phát sinh (nếu có).</span></p>
<p><span style="font-size: medium;">2 - Phạm vi sử dụng thông tin cá nhân</span></p>
<p><span style="font-size: medium;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Website vongbimientrung.com sử dụng thông tin của Khách hàng cung cấp để:</span></p>
<ul>
<li><span style="font-size: medium;">Cung cấp các dịch vụ đến Khách hàng;</span></li>
<li><span style="font-size: medium;">Gửi các thông báo về các hoạt động trao đổi thông tin giữa Khách hàng và MTC Việt Nam;</span></li>
<li><span style="font-size: medium;">Ngăn ngừa các hoạt động phá hủy, chiếm đoạt tài khoản người dùng của Khách hàng hoặc các hoạt động giả mạo Khách hàng;</span></li>
<li><span style="font-size: medium;">Liên lạc và giải quyết khiếu nại với Khách hàng;</span></li>
<li><span style="font-size: medium;">Xác nhận và trao đổi thông tin về giao dịch của Khách hàng tại MTC Việt Nam;</span></li>
<li><span style="font-size: medium;">Trong trường hợp có yêu cầu của cơ quan quản lý nhà nước có thẩm quyền.</span></li>
</ul>
<p><span style="font-size: medium;">3 - Thời gian lưu trữ thông tin cá nhân</span></p>
<p><span style="font-size: medium;">Không có thời hạn ngoại trừ trường hợp Khách hàng gửi có yêu cầu hủy bỏ tới cho Ban quản trị hoặc Công ty giải thể hoặc bị phá sản.</span></p>
<p><span style="font-size: medium;">4 - Những người hoặc tổ chức có thể được tiếp cận với thông tin cá nhân của khách hàng</span></p>
<p><span style="font-size: medium;">Khách hàng đồng ý rằng, trong trường hợp cần thiết, các cơ quan/ tổ chức/cá nhân sau có quyền được tiếp cận và thu thập các thông tin cá nhân của mình, bao gồm:</span></p>
<ul>
<li><span style="font-size: medium;">Ban quản trị.</span></li>
<li><span style="font-size: medium;">Bên thứ ba có dịch vụ tích hợp với Website vongbimientrung.com</span></li>
<li><span style="font-size: medium;">Công ty tổ chức sự kiện và nhà tài trợ</span></li>
<li><span style="font-size: medium;">Cơ quan nhà nước có thẩm quyền trong trường hợp có yêu cầu theo quy định tại quy chế hoạt động</span></li>
<li><span style="font-size: medium;">Cố vấn tài chính, pháp lý và Công ty kiểm toán</span></li>
<li><span style="font-size: medium;">Bên khiếu nại chứng minh được hành vi vi phạm của Khách hàng</span></li>
<li><span style="font-size: medium;">Theo yêu cầu của cơ quan nhà nước có thẩm quyền</span></li>
</ul>
<p><span style="font-size: medium;">5 - Địa chỉ của đơn vị thu thập và quản lý thông tin</span></p>
<p><span style="font-size: medium;"><strong>Công Ty Cổ phần Thương mại và Công nghệ MTC Việt Nam</strong></span></p>
<ul>
<li><span style="font-size: medium;">Địa chỉ: 04 Trần Kế Xương, Tổ 10, Phường Hải Châu II, Quận Hải Châu, Thành phố Đà Nẵng, Việt Nam</span></li>
<li><span style="font-size: medium;">Điện thoại: 0982488345 - 0913665565 - 0397900540</span></li>
<li><span style="font-size: medium;">Email: support@vongbimientrung.com</span></li>
</ul>
<p><span style="font-size: medium;">6 - Phương tiện và công cụ để khách hàng tiếp cận và chỉnh sửa dữ liệu thông tin cá nhân của mình</span></p>
<p><span style="font-size: medium;">Khách hàng có quyền tự kiểm tra, cập nhật, điều chỉnh hoặc hủy bỏ thông tin cá nhân của mình bằng cách đăng nhập vào Website vongbimientrung.com và chỉnh sửa thông tin cá nhân hoặc yêu cầu Ban quản trị thực hiện việc này.</span></p>
<p><span style="font-size: medium;">Khách hàng có quyền gửi khiếu nại về việc lộ thông tin cá nhân của mình cho bên thứ 3 đến Ban quản trị. Khi tiếp nhận những phản hồi này, MTC Việt Nam sẽ xác nhận lại thông tin, phải có trách nhiệm trả lời lý do và hướng dẫn Khách hàng khôi phục và bảo mật lại thông tin.</span></p>
<p><span style="font-size: medium;">Các hình thức tiếp nhận thông tin khiếu nại của Khách hàng:</span></p>
<p><span style="font-size: medium;"><strong>Qua email:</strong>&nbsp;support@vongbimientrung.com</span></p>
<p><span style="font-size: medium;"><strong>Qua điện thoại:</strong>&nbsp;0982488345 - 0913665565 - 0397900540</span></p>                </div>
            </div>
        </div>
    </div>
</div>
@endsection
