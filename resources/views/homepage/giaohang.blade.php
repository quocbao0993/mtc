@extends('master')
@section('content')
<div class="about_section">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12">
                <div class="about_content">
                    <p style="text-align: center;"><strong><span style="font-size: large;">THÔNG TIN VỀ GIAO HÀNG VÀ ĐỔI TRẢ HÀNG</span></strong></p>
<p style="text-align: center;"><br><br></p>
<p style="text-align: left;"><strong><span style="font-size: medium;">a. Phương thức giao hàng</span></strong></p>
<p><span style="font-size: medium;">Công ty MTC áp dụng linh hoạt các phương thức giao hàng cho Quý khách hàng bao gồm: vận chuyển bằng xe máy, vận chuyển bằng xe tải, gửi hàng qua dịch vụ chuyển phát nhanh…Quý khách hàng vui lòng liên hệ trước với chúng tôi để chọn phương án vận chuyển phù hợp</span><br><span style="font-size: medium;">Thời gian ước tính cho việc giao hàng</span></p>
<p><span style="font-size: medium;">Thời gian giao hàng ước tính tùy thuộc vào phương thức vận chuyển và vị trí địa lý, và tình hình kinh tế xã hội tại thời điểm giao hàng. Chúng tôi cam kết nỗ lực hỗ trợ giao hàng cho khách hàng trong thời gian sớm nhất.</span><br><span style="font-size: medium;">Các giới hạn về mặt địa lý cho việc giao hàng</span></p>
<p><span style="font-size: medium;">Vận chuyển tận nơi bằng xe máy/ chuyển phát nhanh được áp dụng với các đơn trong phạm vi dưới 20km.<strong></strong></span></p>
<p><span style="font-size: medium;">Vận chuyển bằng xe tải/ chuyển phát nhanh được áp dụng với các đơn hàng trên 100 kg hoặc các đơn hàng với khoảng cách trên 20km.</span></p>
<p><span style="font-size: medium;">Hình thức vận chuyển và chi phí vận chuyển sẽ do hai bên thỏa thuận trước khi giao hàng.</span></p>
<p><span style="font-size: medium;">Trường hợp phát sinh chậm trễ trong việc giao hàng, chúng tôi sẽ cập nhật ngay thông tin kịp thời cho khách hàng và hoặc quý khách có thể liên hệ nhận thông tin tiến độ nhận hàng qua nhân viên phụ trách bán hàng hoặc qua số Điện thoại: 0982 488 345 - 0913 66 55 65 - 0397900540  , Email: support@vongbimientrung.com</span></p>
<p style="text-align: left;"><span style="font-size: medium;"><br></span></p>
<p style="text-align: left;"><strong><span style="font-size: medium;">b. </span><a style="font-size: medium;" href="https://abada.vn/pages/chinh-sach-doi-tra">Chính sách đổi trả</a></strong></p>
<p style="text-align: left;"><strong style="font-size: medium;">1. Điều kiện đổi trả</strong></p>
<p style="text-align: left;"><span style="font-size: medium;">Quý Khách hàng cần kiểm tra tình trạng hàng hóa và có thể đổi hàng/ trả lại hàng&nbsp;ngay tại thời điểm giao/nhận hàng&nbsp;trong những trường hợp sau:</span><span style="font-size: medium;">&nbsp;</span></p>
<ul style="text-align: left;">
<li><span style="font-size: medium;">Hàng không đúng chủng loại, mẫu mã trong đơn hàng đã đặt hoặc như trên website tại thời điểm đặt hàng.</span></li>
<li><span style="font-size: medium;">Không đủ số lượng, không đủ bộ như trong đơn hàng.</span></li>
<li><span style="font-size: medium;">Tình trạng bên ngoài bị ảnh hưởng như rách bao bì, bong tróc, bể vỡ…</span>&nbsp;</li>
</ul>
<p style="text-align: left;"><span style="font-size: medium;"><strong>2. Quy định về thời gian thông báo và gửi sản phẩm đổi trả</strong></span></p>
<p style="text-align: left;"><strong style="font-size: medium;">Thời gian thông báo đổi trả</strong><span style="font-size: medium;">:&nbsp;trong vòng 48h kể từ khi nhận sản phẩm đối với trường hợp sản phẩm thiếu phụ kiện, quà tặng hoặc bể vỡ.<br></span><strong style="font-size: medium;">Thời gian gửi chuyển trả sản phẩm</strong><span style="font-size: medium;">: trong vòng 3 ngày kể từ khi nhận sản phẩm.<br></span><strong style="font-size: medium;">Địa điểm đổi trả sản phẩm</strong><span style="font-size: medium;">: Khách hàng có thể mang hàng trực tiếp đến văn phòng/ kho&nbsp;của chúng tôi hoặc chuyển qua đường bưu điện.</span></p>
<p style="text-align: left;"><span style="font-size: medium;">&nbsp;</span><span style="font-size: medium;">Trong trường hợp Quý Khách hàng có ý kiến đóng góp/ khiếu nại liên quan đến chất lượng sản phẩm, Quý Khách hàng vui lòng liên hệ đường dây chăm sóc khách hàng&nbsp;của chúng tôi.</span></p>
<p style="text-align: left;"><span style="font-size: medium;"><strong><br></strong></span></p>  </div>
            </div>
        </div>
    </div>
</div>
@endsection
