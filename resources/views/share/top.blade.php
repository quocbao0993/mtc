<div class="site_wrapper">
    <div class="topbar dark topbar-padding">
      <div class="container">
        <div class="topbar-left-items">
          <ul class="toplist toppadding pull-left paddtop1">
            <li class="rightl">Chăm sóc khách hàng - HOTLINE:</li>
            <li>0982488345 - 0913665565 - 0397900540</li>

            {{-- <li>EMAIL: XXX</li> --}}

          </ul>
        </div>
        <!--end left-->

        <div class="topbar-right-items pull-right">
          <ul class="toplist toppadding">
            {{-- <li><a href="#"><i class="fa fa-search"></i> &nbsp;</a></li> --}}
            <li>ĐẠI LÝ ỦY QUYỀN SKF TẠI VIỆT NAM</li>
            <br>
            <li><i class="fa fa-envelope"> Email: email@vongbimientrung.com</i></li>
                                                                                                                                                                                                               </ul>
        </div>
      </div>
    </div>
    <div class="topbar white">
      <div class="container">
        <div class="topbar-left-items">
          <div class="margin-top1"></div>
          <ul class="toplist toppadding pull-left paddtop1">
            <li class="lineright"><a href="/#">Đăng nhập</a></li>
            <li><a href="/#">Đăng ký</a></li>
          </ul>
         </div>
        <!--end left-->

        <div class="topbar-middle-logo no-bgcolor"><a href="/#"><img src="/assets/images/logo.png" alt=""/></a></div>

        <!--end middle-->

        <div class="topbar-right-items pull-right">
          <div class="margin-top1"></div>
          <ul class="toplist toppadding">
            <li class="lineright"><a href="/#">Tìm kiếm</a></li>
            <li class="last"><a href="/contact">Nhận báo giá</a></li>
            {{-- <li class="last"><a href="#">Giỏ hàng</a></li> --}}
          </ul>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

    <div id="header">
      <div class="container">
        <div class="navbar red-2 navbar-default yamm ">
          <div class="navbar-header">
            <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid" class="navbar-toggle two three"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          </div>
          <div id="navbar-collapse-grid" class="navbar-collapse collapse">
            <ul class="nav red-2 navbar-nav">
              <li class="dropdown"> <a href="/#" class="dropdown-toggle active">TRANG CHỦ</a>
              </li>
              <li><a href="/about" class="dropdown-toggle">GIỚI THIỆU</a></li>
              <li class="dropdown yamm-fw"> <a href="/#" class="dropdown-toggle">SẢN PHẨM</a>
                <ul class="dropdown-menu">
                  <li>
                    <!-- Content container to add padding -->
                    <div class="yamm-content">
                      <div class="row">
                        <ul class="col-sm-6 col-md-3 list-unstyled ">
                          <li>
                            <p> SẢN PHẨM SKF CHÍNH HÃNG </p>
                          </li>
                          <li><a href="/vong-bi-skf"><img width="58" height="58" alt="Vòng bi SKF chính hãng" src="https://ngocanh.com/public/uploads/images/5435/vongbi-skf-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; VÒNG BI SKF CHÍNH HÃNG</a></li>
                          <li><a href="/#"><img width="58" height="58" alt="Gối đỡ SKF chính hãng" src="https://ngocanh.com/public/uploads/images/100/goi-do-skf-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; GỐI ĐỠ SKF CHÍNH HÃNG</a></li>
                          <li><a href="/mo-boi-tron"><img width="58" height="58" alt="Mỡ bôi trơn SKF chính hãng" src="https://ngocanh.com/public/uploads/images/103/mo-skf-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; MỠ BÔI TRƠN SKF CHÍNH HÃNG</a></li>
                          <li><a href="/#"><img width="58" height="58" alt="Dây đai SKF chính hãng" src="https://ngocanh.com/public/uploads/images/1363/day-dai-skf-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; DÂY ĐAI SKF CHÍNH HÃNG</a></li>
                          <li><a href="/#"><img width="58" height="58" alt="Phớt chặn dầu SKF chính hãng" src="https://ngocanh.com/public/uploads/images/1364/phot-skf-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; PHỚT CHẶN DẦU SKF CHÍNH HÃNG</a></li>
                        </ul>
                        <ul class="col-sm-6 col-md-3 list-unstyled ">
                          <li>
                            <p> DỤNG CỤ BẢO TRÌ SKF CHÍNH HÃNG </p>
                          </li>
                          <li><a href="/dung-cu"><img width="58" height="58" alt="Dụng cụ bảo trì SKF chính hãng" src="https://ngocanh.com/public/uploads/images/1365/dung-cu-skf-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; DỤNG CỤ BẢO TRÌ SKF CHÍNH HÃNG</a></li>
                        </ul>
                        <ul class="col-sm-6 col-md-3 list-unstyled ">
                          <li>
                            <p> Xích tải SFK CHỈNH HÃNG </p>
                          </li>
                          <li><a href="/#"><img width="58" height="58" alt="Xích tải SKF chính hãng" src="https://ngocanh.com/public/uploads/images/5432/xich-tai-skf-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; XÍCH TẢI SKF CHÍNH HÃNG</a></li>
                        </ul>
                        <ul class="col-sm-6 col-md-3 list-unstyled ">
                          <li>
                            <p> VÒNG BI XE CHÍNH HÃNG </p>
                          </li>
                          <li><a href="/#"><img width="58" height="58" alt="Vòng bi xe máy chính hãng" src="https://ngocanh.com/public/uploads/images/5433/vong-bi-xe-may-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; VÒNG BI XE MÁY SKF CHÍNH HÃNG</a></li>
                          <li><a href="/#"><img width="58" height="58" alt="Vòng bi xe tải chính hãng" src="https://ngocanh.com/public/uploads/images/5434/vong-bi-xe-tai-skf-150x150c.jpg?v=2.5.9" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; VÒNG BI XE TẢI SKF CHÍNH HÃNG</a></li>
                        </ul>
                      </div>
                    </div>
                  </li>
                </ul>
              </li>
              <li class="dropdown yamm-fw"> <a href="/tin-tuc" class="dropdown-toggle">TÀI LIỆU</a>
                <ul class="dropdown-menu">
                  <li>
                    <!-- Content container to add padding -->
                    <div class="yamm-content">
                      <div class="row">
                        <ul class="col-sm-6 col-md-3 list-unstyled ">
                          <li>
                            <a href="/tin-tuc"> Tin tức chung SKF </a>
                          </li>
                          {{-- <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Premium Wear</a></li> --}}
                        </ul>
                        <ul class="col-sm-6 col-md-3 list-unstyled ">
                          <li>
                            <a href="/tu-van"> Tư vấn - Review </a>
                          </li>
                          {{-- <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Formal Shoes</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Sandals & Floaters</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Boots</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Casual Shoes</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Sports Shoes</a></li> --}}
                        </ul>
                        {{-- <ul class="col-sm-6 col-md-3 list-unstyled ">
                          <li>
                            <p> Watches </p>
                          </li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Fasttrack</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Casio</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Timex</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Nixon</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Nine West</a></li>
                        </ul>
                        <ul class="col-sm-6 col-md-3 list-unstyled ">
                          <li>
                            <p> Accessories </p>
                          </li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Bags</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Belts</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Sunglasses</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Shaving Kits & Needs</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Bath Needs</a></li>
                        </ul> --}}
                      </div>
                    </div>
                  </li>
                </ul>
              </li>
              <li><a href="/catalogue" class="dropdown-toggle">CATALOGUE</a></li>

              <li><a href="/contact" class="dropdown-toggle">LIÊN HỆ</a></li>
            </ul>
            <br/>
            <a href="/contact" class="dropdown-toggle pull-right btn btn-red btn-xround">GỌI NGAY</a> </div>
        </div>
      </div>
    </div>
