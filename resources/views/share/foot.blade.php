<section class="section-light sec-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-3 clearfix">
          <div class="footer-logo"><img width="200" height="58" src="/assets/images/flogo.png" alt=""></div>
          <ul class="address-info-2">
            <li>Address: 04 Trần Kế Xương, Quận Hải Châu, Thành phố Đà Nẵng</li>
            <li><i class="fa fa-phone"></i> Phone 1: 0982488345 </li>
            <li><i class="fa fa-phone"></i> Phone 2: 0913665565 </li>
            <li><i class="fa fa-phone"></i> Phone 2: 0397900540 </li>
            <li><i class="fa fa-envelope"></i> Email: support@vongbimientrung.com </li>
          </ul>
        </div>
        <!--end item-->


        <div class="col-md-3 clearfix">
            <h4 class="uppercase footer-title two less-mar3 roboto-slab">TÀI LIỆU SKF</h4>
            <div class="clearfix"></div>
            <div class="footer-title-bottomstrip"></div>
            <ul class="usefull-links-2">
              <li><a href="/#"><i class="fa fa-angle-right"></i> Đại lý ủy quyền SKF</a></li>
              <li><a href="/#"><i class="fa fa-angle-right"></i> Catalogue công ty</a></li>
              <li><a href="/phan-biet-hang-gia"><i class="fa fa-angle-right"></i> Phân biệt vòng bi giả</a></li>
              {{-- <li><a href="#"><i class="fa fa-angle-right"></i> Tư vấn lựa chọn vòng bi</a></li>
              <li><a href="/giao-hang"><i class="fa fa-angle-right"></i> Giao hàng và đổi trả hàng</a></li> --}}
            </ul>
          </div>
        <!--end item-->

        <div class="col-md-3 clearfix">
          <h4 class="uppercase footer-title two less-mar3 roboto-slab">VỀ CHÚNG TÔI</h4>
          <div class="clearfix"></div>
          <div class="footer-title-bottomstrip"></div>
          <ul class="usefull-links-2">
            <li><a href="#"><i class="fa fa-angle-right"></i> Đại lý ủy quyền SKF</a></li>
            <li><a href="/about"><i class="fa fa-angle-right"></i> Giới thiệu chung</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Dịch vụ kỹ thuật</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Tư vấn lựa chọn vòng bi</a></li>
            <li><a href="/giao-hang"><i class="fa fa-angle-right"></i> Giao hàng và đổi trả hàng</a></li>
          </ul>
        </div>
        <!--end item-->
        <div class="col-md-3 clearfix">
            <h4 class="uppercase footer-title two less-mar3 roboto-slab">THÔNG TIN CHUNG</h4>
            <div class="clearfix"></div>
            <div class="footer-title-bottomstrip"></div>
            <ul class="usefull-links-2">
              <li><a href="/bao-mat"><i class="fa fa-angle-right"></i> Chính sách bảo mật</a></li>
              <li><a href="/giao-hang"><i class="fa fa-angle-right"></i> Bảo hành</a></li>
              <li><a href="/bao-mat"><i class="fa fa-angle-right"></i> Câu hỏi thường gặp</a></li>
              <li><a href="/thanh-toan"><i class="fa fa-angle-right"></i> Phương thức thanh toán</a></li>
              <li><a href="/contact"><i class="fa fa-angle-right"></i> Liên hệ</a></li>
            </ul>
          </div>
        {{-- <div class="col-md-3 clearfix">
          <div class="item-holder">
            <h4 class="uppercase footer-title two less-mar3 roboto-slab">Featured</h4>
            <div class="footer-title-bottomstrip"></div>
            <div class="clearfix"></div>
            <div class="image-left"><img src="http://placehold.it/80x80" alt=""></div>
            <div class="text-box-right">
              <h6 class="less-mar3 nopadding roboto-slab nopadding"><a href="#">Clean And Modern</a></h6>
              <p>$12.59</p>
              <div class="footer-post-info"><span><i class="fa fa-star"></i></span><span><i class="fa fa-star"></i></span><span><i class="fa fa-star"></i></span><span><i class="fa fa-star"></i></span><span><i class="fa fa-star"></i></span></div>
            </div>
            <div class="divider-line solid light margin"></div>
            <div class="clearfix"></div>
            <div class="image-left"><img src="http://placehold.it/80x80" alt=""></div>
            <div class="text-box-right">
              <h6 class="less-mar3 roboto-slab nopadding"><a href="#">Layered PSD Files</a></h6>
              <p>Lorem ipsum dolor sit</p>
              <div class="footer-post-info"><span><i class="fa fa-star"></i></span><span><i class="fa fa-star"></i></span><span><i class="fa fa-star"></i></span><span><i class="fa fa-star"></i></span><span><i class="fa fa-star"></i></span></div>
            </div>
          </div>
        </div> --}}
        <!--end item-->

      </div>
    </div>
  </section>
  <!--end section-->
  <div class="clearfix"></div>
  <a href="#" class="scrollup red-4"></a><!-- end scroll to top of the page-->
